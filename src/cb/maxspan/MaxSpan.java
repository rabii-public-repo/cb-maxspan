package cb.maxspan;

import java.util.TreeSet;

/*
 * @see https://codingbat.com/prob/p189576
 */
public class MaxSpan {
	public static void main(String[] args) {
		int nums[] = { 3, 9 };
		System.out.println(getLeftMost(0, nums));
		System.out.println(maxSpan(nums));
	}

	static int maxSpan(int[] nums) {
		TreeSet<Integer> treeSet = new TreeSet<>();
		for (int i = 0; i < nums.length; i++) {
			treeSet.add(getLeftMost(i, nums));
		}
		return treeSet.last();
	}

	static int getLeftMost(int indexVal, int[] nums) {
		int leftMost = nums.length - 1;
		while (indexVal < leftMost) {
			if (nums[indexVal] == nums[leftMost]) {
				return (leftMost + 1) - (indexVal);
			}
			leftMost--;
		}
		return 1;
	}
}
